# Addonis - The better add-ons registry.
Graduate project of **Hristian Ivanov** and **Ivan Ivanov**.
## Description

**Addonis is an Addons Registry web application.Allows users to download addons that are appropriate for their IDE, rate addons uploaded by others and upload their own addons.**

Some of the possible actions it enables its **anonymous** users to do are:

**·** Login and register.

**·** Browse addons for their preferred IDE.

**·** Search addon by name.

**·** Sort addons by name, count of downloads, upload date and last commit date.

**·** Download addons.

Some of the possible actions it enables its **enabled** users to do are:

**·** First, they must first verify their accounts via email if they want to have rights and be able to upload addons.

**·** Manage their own profiles and addons.

**·** Browse addons for their preferred IDE.

**·** Sort addons by name, count of downloads, upload date and last commit date.

**·** Download addons.

**·** Rate existing addons.

**·** Invite а friend to register.

Some of the possible actions it enables its **admins** to do are:

**·** See a list of users and search them by phone number, username or email.

**·** Block/unblock users.

**·** Approve pending addons.

**·** Delete addons.

**·** Modify addons.

## Technologies

**·** Spring
**·** SpringMVC
**·** Hibernate
**·** Thymeleaf
**·** HTML
**·** CSS
**·** JUnit
**·** MariaDB

## Swagger documentation

- [Click here for Swagger doccumentation](http://localhost:8080/swagger-ui.html#/)

## Database

![scheme](/images/DB.PNG)


## Home page

![scheme](/images/HomePage.PNG)

![scheme](/images/HomePage2.PNG)


## All Addons

![scheme](/images/AllAddons.PNG)


***



